package com.example.andrehaas.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.andrehaas.myapplication.entidade.Pessoa;
import com.example.andrehaas.myapplication.repository.PessoaRepository;
import com.example.andrehaas.myapplication.util.TipoMsg;
import com.example.andrehaas.myapplication.util.util;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ListaPessoaActivity extends AppCompatActivity {

    private ListView lstPessoa;
    private PessoaRepository repository;
    private int posicaoSelecionada;

    private ArrayAdapter adapter;

    private List<Pessoa> ListaPessoa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pessoa);

        getSupportActionBar().setTitle("Listagem de Pessoa");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        lstPessoa = (ListView) findViewById(R.id.lstPessoas);
        repository = new PessoaRepository(this);
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1);

        setArrayAdapterPessoas();
        lstPessoa.setOnItemClickListener(clickListenerPessoa);
        lstPessoa.setOnCreateContextMenuListener(contextMenuListener);
        lstPessoa.setOnItemLongClickListener(onItemLongClickListener);


    }

    private void setArrayAdapterPessoas() {

        ListaPessoa = repository.listarPessoa();

        List<String> valores = new ArrayList<String>();

        for (Pessoa p : ListaPessoa) {
            valores.add(p.getNome());
        }

        adapter.clear();
        adapter.addAll(valores);
        lstPessoa.setAdapter(adapter);
    }

    private AdapterView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

            posicaoSelecionada = position;

            return false;
        }
    };

    private View.OnCreateContextMenuListener contextMenuListener = new View.OnCreateContextMenuListener() {
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.setHeaderTitle("Opçoes").setHeaderIcon(R.drawable.settings);
            menu.add(1, 10, 1, "Editar");
            menu.add(1, 20, 2, "Excluir");
        }
    };


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 10:
                Pessoa pessoa = repository.consultarPessoaPorId(ListaPessoa.get(posicaoSelecionada).getIdPessoa());
                Intent i = new Intent(this, EditarPessoaActivity.class);
                i.putExtra("pessoa", pessoa);
                startActivity(i);
                finish();
                break;
            case 20:
                util.showMsgConfirm(ListaPessoaActivity.this, "Remover Pessoa", "Deseja realmente remover \"" + ListaPessoa.get(posicaoSelecionada).getNome().toUpperCase() + "\"?", TipoMsg.ALERTA, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        repository.removerPessoaPorId(ListaPessoa.get(posicaoSelecionada).getIdPessoa());
                        setArrayAdapterPessoas();
                        adapter.notifyDataSetChanged();
                        util.showMsgToast(ListaPessoaActivity.this, "Usuario Removido !");
                    }
                });
                break;
        }
        return true;
    }

    private AdapterView.OnItemClickListener clickListenerPessoa = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Pessoa pessoa = repository.consultarPessoaPorId(ListaPessoa.get(position).getIdPessoa());

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


            StringBuilder info = new StringBuilder();
            info.append("Nome : " + pessoa.getNome());
            info.append("\nEndereço : " + pessoa.getEndereco());
            info.append("\nCPF/CNPJ : " + pessoa.getCpfCnpj());
            info.append("\nSexo : " + pessoa.getSexo().getDescricao());
            info.append("\nProfissão : " + pessoa.getProfissao().getDescricao());
            info.append("\nDt. Nasc : " + dateFormat.format(pessoa.getDtNasc()));

            util.showMsgAlert(ListaPessoaActivity.this, "Detalhes", info.toString(), false);

        }
    };


    public void addNewPessoa(View view) {
        Intent i = new Intent(this, PessoaActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;


    }

}
