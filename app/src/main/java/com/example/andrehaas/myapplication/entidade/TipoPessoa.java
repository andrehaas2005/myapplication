package com.example.andrehaas.myapplication.entidade;

/**
 * Created by andre.haas on 03/01/2017.
 */

public enum TipoPessoa {
    FISICA,JURIDICA
}
