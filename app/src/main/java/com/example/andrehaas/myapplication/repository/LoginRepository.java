package com.example.andrehaas.myapplication.repository;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.andrehaas.myapplication.util.Constantes;
import com.example.andrehaas.myapplication.util.util;

/**
 * Created by andre.haas on 30/12/2016.
 */

public class LoginRepository extends SQLiteOpenHelper{



    public LoginRepository(Context context) {
        super(context, Constantes.BD_NOME, null, Constantes.BD_VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        StringBuilder query = new StringBuilder();
        query.append("CREATE TABLE  TB_LOGIN( ");
        query.append(" ID_LOGIN INTEGER PRIMARY KEY AUTOINCREMENT, ");
        query.append(" USUARIO TEXT(15) NOT NULL , ");
        query.append(" SENHA TEXT(15) NOT NULL ) ");
        db.execSQL(query.toString());
        popularBD(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void popularBD(SQLiteDatabase db){
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO TB_LOGIN(USUARIO, SENHA) VALUES(?,?)");

        //String[] params = {"admin","admin"};
        //db.execSQL(query.toString(),params); //ou
        db.execSQL(query.toString(),new String[] {"admin","admin"});

    }

    public void listarLogin(Activity activity){
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("TB_LOGIN",null,"id_login=?",new String[]{"1"},null,null,"USUARIO");
        while(cursor.moveToNext()) {
      String txt = ("Nome do usuario: " + cursor.getString(cursor.getColumnIndex("USUARIO")));
        Log.d("Senha do usuario",cursor.getString(cursor.getColumnIndex("SENHA")));
            util.showMsgToast(activity,txt);
        }
    }

    public void addLogin(String login, String senha){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("USUARIO",login);
        contentValues.put("SENHA",senha);
        db.insert("TB_LOGIN",null,contentValues);
    }

    public void updateLogin(String login,String senha){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("SENHA",senha);

        db.update("TB_LOGIN",contentValues,"USUARIO=?",new String[]{login});

    }

    public void deleteLogin(String login){
        SQLiteDatabase db = getWritableDatabase();
        db.delete("TB_LOGIN","USUARIO=?",new String[]{login});
    }
}
