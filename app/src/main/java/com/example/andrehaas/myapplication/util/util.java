package com.example.andrehaas.myapplication.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andrehaas.myapplication.R;

/**
 * Created by andre.haas on 29/12/2016.
 */

public class util {

    public static void showMsgToast(Activity activity, String msg) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layoutToast = inflater.inflate(R.layout.toast_template, (ViewGroup) activity.findViewById(R.id.lyToast));

        TextView txtToast = (TextView) layoutToast.findViewById(R.id.txtToast);
        txtToast.setText(msg);
        Toast toast = new Toast(activity);
        toast.setView(layoutToast);
        //toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    public static void showMsgConfirm(final Activity activity, String titulo, String txt, TipoMsg tipoMsg,DialogInterface.OnClickListener listener) {
        int theme = 0, icone = 0;
        switch (tipoMsg) {
            case SUCESSO:
                theme = R.style.AppTheme_Dark_Dialog_Sucesso;
                icone = R.drawable.success;
                break;
            case INFO:
                theme = R.style.AppTheme_Dark_Dialog_Info;
                icone = R.drawable.info;
                break;
            case ERRO:
                theme = R.style.AppTheme_Dark_Dialog_Error;
                icone = R.drawable.error;
                break;
            case ALERTA:
                theme = R.style.AppTheme_Dark_Dialog_Alert;
                icone = R.drawable.alert;
                break;
        }
        final AlertDialog alertDialog = new AlertDialog.Builder(activity, theme).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(txt);
        alertDialog.setIcon(icone);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", listener);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    public static void showMsgAlertOK(final Activity activity, String titulo, String txt, TipoMsg tipoMsg) {
        int theme = 0, icone = 0;
        switch (tipoMsg) {
            case SUCESSO:
                theme = R.style.AppTheme_Dark_Dialog_Sucesso;
                icone = R.drawable.success;
                break;
            case INFO:
                theme = R.style.AppTheme_Dark_Dialog_Info;
                icone = R.drawable.info;
                break;
            case ERRO:
                theme = R.style.AppTheme_Dark_Dialog_Error;
                icone = R.drawable.error;
                break;
            case ALERTA:
                theme = R.style.AppTheme_Dark_Dialog_Alert;
                icone = R.drawable.alert;
                break;
        }
        final AlertDialog alertDialog = new AlertDialog.Builder(activity, theme).create();
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(txt);
        alertDialog.setIcon(icone);

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.dismiss();
            }
        });
    }
        public static void showMsgAlert ( final Activity activity, String titulo, String msg,
        boolean btn_cancelar){
            final AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
            alertDialog.setIcon(R.drawable.info48);
            alertDialog.setTitle(titulo);
            alertDialog.setMessage(msg);
            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //         util.showMsgToast(activity,"Nova conexao realizada...");
                    alertDialog.dismiss();
                }
            });
            if (btn_cancelar) {
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
            alertDialog.show();
        }
    }
