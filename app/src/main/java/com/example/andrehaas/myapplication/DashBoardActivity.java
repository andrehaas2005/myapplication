package com.example.andrehaas.myapplication;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DashBoardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        getSupportActionBar().hide();
    }

    public void callCadastro(View view) {
        Intent i = new Intent(this, PessoaActivity.class);
        startActivity(i);

    }

    public void callList(View view) {

        Intent i = new Intent(this, ListaPessoaActivity.class);
        startActivity(i);
    }
}
