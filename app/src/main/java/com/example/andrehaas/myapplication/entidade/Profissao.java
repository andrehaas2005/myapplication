package com.example.andrehaas.myapplication.entidade;

/**
 * Created by andre.haas on 02/01/2017.
 */

public enum Profissao {
    ARQUITETO("Arquiteto de Software"),PEDREIRO("Pedreiro"),PROFESSOR("Professor"),DESENVOLVEDOR("Desenvolvedor de Sistemas"),
    ANALISTA("Analista de Sistema"),ENGENHEIRO("Engenheiro"),ZELADOR("Zelador"),CARTEIRO("Carteiro");

    private Profissao(String descricao){
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public static Profissao getProfissao(int pos){
        for (Profissao p : Profissao.values()){
            if(p.ordinal()==pos){
                return p;
            }
        }
        return null;
    }
}
