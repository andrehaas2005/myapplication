package com.example.andrehaas.myapplication;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.andrehaas.myapplication.entidade.Pessoa;
import com.example.andrehaas.myapplication.entidade.Profissao;
import com.example.andrehaas.myapplication.entidade.Sexo;
import com.example.andrehaas.myapplication.entidade.TipoPessoa;
import com.example.andrehaas.myapplication.fragment.DatePickerFragment;
import com.example.andrehaas.myapplication.repository.PessoaRepository;
import com.example.andrehaas.myapplication.util.Mask;
import com.example.andrehaas.myapplication.util.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EditarPessoaActivity extends AppCompatActivity {

    private Pessoa pessoa;

    private Spinner spnProfissoa;
    private EditText edtCpfCnpj, edtNome, edtEdereco;
    private RadioGroup rbgCpfCnpj, rbgSexo;
    private RadioButton rbtCpf,rbtCnpj,rbtMasculino,rbtFeminino;
    private TextView txtCpfCnpj, edtNasc;
    private TextWatcher cpfMask, cnpjMask;
    private int cpfCnpjSelecionado;
    private PessoaRepository pessoaRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_pessoa);

        getSupportActionBar().setTitle("Editar Pessoa");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        pessoa = (Pessoa) getIntent().getExtras().getSerializable("pessoa");
        this.pessoaRepository = new PessoaRepository(this);
        spnProfissoa = (Spinner) findViewById(R.id.spnProfissao);
        edtCpfCnpj = (EditText) findViewById(R.id.edtCpfCnpj);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtEdereco = (EditText) findViewById(R.id.edtEndereco);
        rbgCpfCnpj = (RadioGroup) findViewById(R.id.rbgCpfCnpj);
        rbgSexo = (RadioGroup) findViewById(R.id.rbgSexo);
        rbtCpf = (RadioButton) findViewById(R.id.rbtCpf);
        rbtCnpj = (RadioButton) findViewById(R.id.rbtCnpj);
        rbtMasculino = (RadioButton) findViewById(R.id.rbtMasculino);
        rbtFeminino = (RadioButton) findViewById(R.id.rbtFeminino);
        txtCpfCnpj = (TextView) findViewById(R.id.txtCpfCnpj);
        edtNasc = (TextView) findViewById(R.id.edtNasc);

        cpfMask = Mask.insert("###.###.###-##", edtCpfCnpj);
        edtCpfCnpj.addTextChangedListener(cpfMask);
        cnpjMask = Mask.insert("##.###.###/####-##", edtCpfCnpj);

        rbgCpfCnpj.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                edtCpfCnpj.setText("");
                cpfCnpjSelecionado = group.getCheckedRadioButtonId();
                if (cpfCnpjSelecionado == rbtCpf.getId()) {
                    txtCpfCnpj.setText("CPF:");
                    edtCpfCnpj.removeTextChangedListener(cnpjMask);
                    edtCpfCnpj.addTextChangedListener(cpfMask);
                } else {
                    txtCpfCnpj.setText("CNPJ:");
                    edtCpfCnpj.removeTextChangedListener(cpfMask);
                    edtCpfCnpj.addTextChangedListener(cnpjMask);
                }
            }
        });
        edtCpfCnpj.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (rbgCpfCnpj.getCheckedRadioButtonId() == rbtCpf.getId()) {
                    if (edtCpfCnpj.getText().length() < 14) {
                        edtCpfCnpj.setText("");
                    }
                } else {
                    if (edtCpfCnpj.getText().length() < 18) {
                        edtCpfCnpj.setText("");
                    }
                }
            }
        });


        this.initProfissoes();
        this.initCampos();


    }

    public void setData(View view) {
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        Calendar cal = Calendar.getInstance();

        Bundle bundle = new Bundle();
        bundle.putInt("dia", cal.get(Calendar.DAY_OF_MONTH));
        bundle.putInt("mes", cal.get(Calendar.MONTH));
        bundle.putInt("ano", cal.get(Calendar.YEAR));
        datePickerFragment.setArguments(bundle);
        datePickerFragment.setDateListener(dateListener);
        datePickerFragment.show(getFragmentManager(), "Data Nasc.");
    }


    private DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            edtNasc.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
        }
    };

    private void initProfissoes() {
        ArrayList<String> profissoes = new ArrayList<>();
        for (Profissao p : Profissao.values()) {
            profissoes.add(p.getDescricao());
        }
        ArrayAdapter adapter = new ArrayAdapter(EditarPessoaActivity.this, android.R.layout.simple_spinner_item, profissoes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProfissoa.setAdapter(adapter);
    }

    private void initCampos() {
        edtNome.setText(pessoa.getNome());
        edtEdereco.setText(pessoa.getEndereco());


        switch (pessoa.getTipoPessoa()){
            case FISICA:
                txtCpfCnpj.setText("CPF:");
                rbtCpf.setChecked(true);
                break;

            case JURIDICA:
                txtCpfCnpj.setText("CNPJ:");
                rbtCnpj.setChecked(true);
                break;
        }
        edtCpfCnpj.setText(pessoa.getCpfCnpj());
        switch (pessoa.getSexo()){
            case MASCULINO:
                rbtMasculino.setChecked(true);
                rbtFeminino.setChecked(false);
                break;
            case FEMININO:
                rbtMasculino.setChecked(false);
                rbtFeminino.setChecked(true);
                break;
        }

        spnProfissoa.setSelection(pessoa.getProfissao().ordinal());

        SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
       edtNasc.setText(data.format(pessoa.getDtNasc()));
    }

    private Pessoa montarPessoa() {
        Pessoa pessoa = new Pessoa();
        pessoa.setIdPessoa(this.pessoa.getIdPessoa());
        pessoa.setNome(edtNome.getText().toString());
        pessoa.setEndereco(edtEdereco.getText().toString());
        pessoa.setCpfCnpj(edtCpfCnpj.getText().toString());
        switch (rbgCpfCnpj.getCheckedRadioButtonId()) {
            case R.id.rbtCpf:
                pessoa.setTipoPessoa(TipoPessoa.FISICA);
                break;
            case R.id.rbtCnpj:
                pessoa.setTipoPessoa(TipoPessoa.JURIDICA);
                break;
        }

        switch (rbgSexo.getCheckedRadioButtonId()) {
            case R.id.rbtMasculino:
                pessoa.setSexo(Sexo.MASCULINO);

                break;
            case R.id.rbtFeminino:
                pessoa.setSexo(Sexo.FEMININO);
                break;
        }

        Profissao profissao = Profissao.getProfissao(spnProfissoa.getSelectedItemPosition());
        pessoa.setProfissao(profissao);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date nasc = dateFormat.parse(edtNasc.getText().toString());
            pessoa.setDtNasc(nasc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return pessoa;
    }

    private boolean validarPessoa(Pessoa pessoa) {
        boolean erro = false;


        if (pessoa.getNome() == null || "".equals(pessoa.getNome())) {
            erro = true;
            edtNome.setError("Campo Nome Obrigatorio !");
        }
        if (pessoa.getEndereco() == null || "".equals(pessoa.getEndereco())) {
            erro = true;
            edtEdereco.setError("Campo Endereço Obrigatorio !");
        }
        if (pessoa.getCpfCnpj() == null || "".equals(pessoa.getCpfCnpj())) {
            switch (rbgCpfCnpj.getCheckedRadioButtonId()) {
                case R.id.rbtCpf:
                    erro = true;
                    edtCpfCnpj.setError("Campo CPF Obrigatorio !");

                    break;
                case R.id.rbtCnpj:
                    erro = true;
                    edtCpfCnpj.setError("Campo CNPJ Obrigatorio !");
                    break;
            }

        } else {
            switch (rbgCpfCnpj.getCheckedRadioButtonId()) {
                case R.id.rbtCpf:
                    if (edtCpfCnpj.getText().length() > 14) {
                        erro = true;
                        edtCpfCnpj.setError("Campo CPF deve ter 11 caracteres !");
                    }
                    break;
                case R.id.rbtCnpj:
                    if (edtCpfCnpj.getText().length() > 18) {
                        erro = true;
                        edtCpfCnpj.setError("Campo CPF deve ter 14 caracteres !");
                    }
                    break;
            }
        }
        if (pessoa.getDtNasc() == null) {
            erro = true;
            edtNasc.setError("Campo Data Nascimento Obrigatorio !");
        }

        return erro;
    }


    public void atualizarPessoa(View view) {

        Pessoa p = montarPessoa();
        if(!validarPessoa(p)){
            pessoaRepository.atualizarPessoa(p);
            Intent i = new Intent(this,ListaPessoaActivity.class);
            startActivity(i);
            finish();
            util.showMsgToast(this, "Dados de "+p.getNome().toUpperCase()+" atualizadas");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
