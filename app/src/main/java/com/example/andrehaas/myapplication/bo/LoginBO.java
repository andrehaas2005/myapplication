package com.example.andrehaas.myapplication.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.andrehaas.myapplication.LoginActivity;
import com.example.andrehaas.myapplication.repository.LoginRepository;
import com.example.andrehaas.myapplication.util.util;
import com.example.andrehaas.myapplication.validation.LoginValidation;

/**
 * Created by andre.haas on 30/12/2016.
 */

public class LoginBO {

    private LoginRepository loginRepository;

    public LoginBO(Activity activity){
        loginRepository = new LoginRepository(activity);
    }

    public boolean validarCamposLogin(LoginValidation validation){
        boolean resultado = true;

        if(validation.getLogin() ==null ||"".equals(validation.getLogin())){
            validation.getEdtLogin().setError("Campo Obrigatorio");
            resultado =  false;
        }

        if(validation.getSenha() ==null||"".equals(validation.getSenha())){
            validation.getEdtSenha().setError("Campo Obrigatorio");
            resultado =  false;
        }

        if(resultado){

           // loginRepository.deleteLogin(validation.getLogin());

            if(!validation.getLogin().equals("admin") || !validation.getSenha().equals("admin")){
                util.showMsgToast(validation.getActivity(),"Login/Senha Inválido !!!");
                resultado = false;
            }else{
                SharedPreferences.Editor editor = validation.getActivity().getSharedPreferences("pref",Context.MODE_PRIVATE).edit();
                editor.putString("login",validation.getLogin());
                editor.putString("senha",validation.getSenha());
                editor.commit();
                resultado =true;

            }
        }


        return resultado;
    }

    public void deslogar(){}

}

